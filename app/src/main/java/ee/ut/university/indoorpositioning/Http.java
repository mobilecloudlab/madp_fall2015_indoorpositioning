package ee.ut.university.indoorpositioning;

import android.os.AsyncTask;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by thorben on 12/10/15.
 */
public class Http {
    private static String SERVER = "http://indoor-position.bitweb.ee:62211";
    public String urlString;
    public String jsonPayload;
    public ImageViewActivity host;


    public void callback(String responsePayload) {
        System.out.println("not the right function");
    }

    public void postPoint(ImageViewActivity host, IPFingerprint calibrationPoint) {
        new PostPoint(host, calibrationPoint);
    }

    public void createNewPath(ImageViewActivity host, int siteId) {
        new CreateNewPath(host, siteId);
    }



    private class PostPoint extends Http {
        public PostPoint(ImageViewActivity host, IPFingerprint point) {
            JSONObject jsonAverageFP = point.toJson();
            String payload = "";
            try {
                payload = "{" +
                        "\"siteId\": " + point.getSiteId() +
                        ", \"pathId\": " + point.getPathId() +
                        ", \"coordinates\": [" + point.getPosition().x + "," + point.getPosition().y + "]" +
                        ", \"type\": \"" + point.getType() + "\"" +
                        ", \"fingerprint\": "+jsonAverageFP.getJSONArray("routers") +
                        "}";
            }
            catch(JSONException e) {
                e.printStackTrace();
            }

            this.host = host;
            this.urlString = SERVER + "/api/point";
            this.jsonPayload = payload;

            new HttpTask(PostPoint.this, "POST").execute();
        }


        public void callback(String responsePayload) {
            System.out.println("posted point");
        }
    }


    private class CreateNewPath extends Http {
        public CreateNewPath(ImageViewActivity host, int siteId) {


            this.host = host;
            this.urlString = SERVER + "/api/path";
            this.jsonPayload = "{\"siteId\": " + siteId + "}";

            new HttpTask(CreateNewPath.this, "POST").execute();
        }


        public void callback(String responsePayload) {
            System.out.println("posted point");
            try {
                JSONObject response = new JSONObject(responsePayload);
                if(response.getInt("statusCode") == HttpURLConnection.HTTP_OK) {
                    JSONObject data = response.getJSONObject("data");

                    host.btnStart.setEnabled(true);
                    host.btnStart.setText("Stop");
                    host.setPrecision(-1);
                    host.currentPathId = data.getInt("id");
                    host.liveMeasurementInProgress = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    private class HttpTask extends AsyncTask<Void, Void, String> {
        private Http host;
        String responsePayload;
        String requestMethod;

        public HttpTask(Http host, String requestMethod) {
            this.host = host;
            responsePayload = "";
            this.requestMethod = requestMethod;
        }

        protected String doInBackground(Void... callback) {
            String responseString = "";
            try {

                URL url = new URL(host.urlString);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod(requestMethod);

                if(requestMethod.equals("POST")) {
                    OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                    wr.write(jsonPayload);
                    wr.flush();

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = con.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }

                        br.close();

                        responseString = sb.toString();

                    } else {
                        System.out.println("HTTP response: " + con.getResponseMessage());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseString;
        }


        protected void onPostExecute(String responseString) {
            host.callback(responseString);
        }
    }
}
