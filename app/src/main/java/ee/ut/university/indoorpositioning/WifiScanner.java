package ee.ut.university.indoorpositioning;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thorben on 17/09/15.
 */
public class WifiScanner extends IntentService {
    WifiManager wifi;
    ArrayList<ScanResult> lastResults;
    int scanInterval = 200;

    public WifiScanner() {
        super("wifiScanner");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                List<ScanResult> results = wifi.getScanResults();
                if(validateScan(results)) {
                    JSONObject data = new JSONObject();
                    JSONArray routers = new JSONArray();
                    try {
                        for(ScanResult sr : results) {
                            JSONObject ap = new JSONObject();
                            ap.put("bssid", sr.BSSID);
                            ap.put("level", Math.abs(sr.level));
                            routers.put(ap);
                        }
                        data.put("routers", routers);
                    }
                    catch(JSONException e) {
                        e.printStackTrace();
                    }
                    Intent i = new Intent("ee.ut.university.indoorpositioning.newScanResults");
                    i.putExtra("data", data.toString());
                    sendBroadcast(i);
                }
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        while(true) {
            long endTime = System.currentTimeMillis() + scanInterval;
            while (System.currentTimeMillis() < endTime) {
                synchronized (this) {
                    try {
                        wait(endTime - System.currentTimeMillis());
                    } catch (Exception e) {
                    }
                }
            }

            wifi.startScan();
        }
    }

    private boolean validateScan(List<ScanResult> results) {
        boolean valid = true;
        if(lastResults != null) {
            if(lastResults.size() == results.size()) {
                boolean identical = true;
                for(int i = 0; i < results.size(); i++) {
                    if(results.get(i).level != lastResults.get(i).level) {
                        identical = false;
                        break;
                    }
                }
                if(identical) {
                    valid = false;
                }
            }
        }

        lastResults = new ArrayList<ScanResult>();
        for(ScanResult sr : results) {
            lastResults.add(sr);
        }

        return valid;
    }
}
